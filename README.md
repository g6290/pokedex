
An app from scratch shows Pokémon information with pictures and skills using GetX state management.

Develop by:
    - Phạm Văn Hào Quang.
    - Trần Hữu Phát.
    - Nguyễn Trần Xuân Hải.

References:
    API:
    
        -https://pokeapi.co/docs/v2.

        -https://raw.githubusercontent.com/azkals47/pokedex/main/pokemons_gen_i.json

    UI/UX:

        -https://dribbble.com/shots/6545819-Pokedex-App/attachments/6545819-Pokedex-App?mode=media
