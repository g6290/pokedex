const Map<String, String> english = {
  'home': 'Home',
  'Power': 'Power',
  'Accuracy':'Accuracy',
  'PP':'PP',
  'MoveView':'MoveView',
  'id: ': 'Id:',
  'Generation Introduced: ': 'Generation Introduced:',
  'Ability': 'Ability',
  'First Ability': 'First Ability',
  'Second Ability': 'Second Ability',
  'Hidden Ability': 'Hidden Ability',
  'Description: ': 'Description:',
};