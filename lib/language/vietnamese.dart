const Map<String, String> vietnamese = {
  'home': 'Trang chủ',
  'Power': 'Năng Lượng',
  'Accuracy':'Chính xác',
  'PP':'PP',
  'MoveView':'MoveView',
  'id: ': 'Mã số:',
  'Generation Introduced: ': 'Thế hệ được giới thiệu:',
  'Ability': 'Ability',
  'First Ability': 'Khả năng đầu tiên',
  'Second Ability': 'Khả năng thứ hai',
  'Hidden Ability': 'Khả năng ẩn',
  'Description: ': 'Mô tả:',
};