class AbilityModel {
  int id;
  String name;
  String description;
  String longDescription;
  String generationIntroduced;
  List<String> pokemonFirstAbility;
  List<String> pokemonSecondAbility;
  List<String> pokemonHiddenAbility;

  AbilityModel({
    required this.id,
    required this.name,
    required this.description,
    required this.longDescription,
    required this.generationIntroduced,
    required this.pokemonFirstAbility,
    required this.pokemonSecondAbility,
    required this.pokemonHiddenAbility,
  });

  factory AbilityModel.fromJson(Map<String, dynamic> json) {
    return AbilityModel(
      id: (json['id'] as int),
      name: (json['name'] as String),
      description: (json['description'] as String),
      generationIntroduced: (json['generationIntroduced'] != null)
          ? (json['generationIntroduced'] as String)
          : '',
      longDescription: (json['longDescription'] != null)
          ? (json['longDescription'] as String)
          : '',
      pokemonFirstAbility: (json['pokemonFirstAbility'] as List)
          .map((first) => first as String)
          .toList(),
      pokemonSecondAbility: (json['pokemonSecondAbility'] != null)
          ? (json['pokemonSecondAbility'] as List)
              .map((second) => second as String)
              .toList()
          : [],
      pokemonHiddenAbility: (json['pokemonHiddenAbility'] != null)
          ? (json['pokemonHiddenAbility'] as List)
              .map((hidden) => hidden as String)
              .toList()
          : [],
    );
  }

  String getNameFormat(String value) =>value[0].toUpperCase()+ value.substring(1, value.length);
}
