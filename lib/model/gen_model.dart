import 'package:flutter/material.dart';

class GenModel {
  String id;
  String name;
  Color color;

  GenModel({
    required this.id,
    required this.name,
    required this.color,
  });
}
