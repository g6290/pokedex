import 'package:pokedex/model/pokemon_model.dart';

class ListPokemonModel {
  String id;
  String name;
  List<PokemonModel> listPokemon;

  ListPokemonModel({
    required this.id,
    required this.name,
    required this.listPokemon,
  });

  factory ListPokemonModel.fromJson(Map<String, dynamic> json) {
    return ListPokemonModel(
      id: json['id'],
      name: json['name'],
      listPokemon: (json['pokemons'] as List)
          .map((e) => PokemonModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );
  }
}
