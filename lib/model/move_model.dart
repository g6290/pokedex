class MoveModel {
  String name;
  String type;
  String category;
  int power;
  double accuracy;
  int pp;

  MoveModel({
    required this.name,
    required this.type,
    required this.category,
    required this.power,
    required this.accuracy,
    required this.pp,
  });

  factory MoveModel.fromJson(Map<String, dynamic> json) {
    return MoveModel(
      name: json['name'] as String,
      type: json['type'] as String,
      category: json['category'] as String,
      power: (json['power'] != null)
          ? json['power'] as int
          : 0,
      accuracy: (json['accuracy'] != null)
          ? json['accuracy'] as double
          : 0,
      pp: (json['pp'] != null)
          ? json['pp'] as int
          : 0,
    );
  }
  String formatName(){
    return name[0].toUpperCase() + name.substring(1,name.length);
  }
  String formatType(){
    return type[0].toUpperCase() + type.substring(1,type.length);
  }
  String formatCategory(){
    return category[0].toUpperCase() + category.substring(1,category.length);
  }
}
