class PokemonModel {
  String id;
  String name;
  String height;
  String weight;
  String imageUrl;
  String category;
  String malePercentage;
  String femalePercentage;
  int hp;
  int attack;
  int defense;
  int specialAttack;
  int specialDefense;
  int speed;
  int total;
  List<String> pokemonTypes;
  List<String> evolutions;

  PokemonModel({
    required this.id,
    required this.name,
    required this.height,
    required this.weight,
    required this.imageUrl,
    required this.category,
    required this.malePercentage,
    required this.femalePercentage,
    required this.hp,
    required this.attack,
    required this.defense,
    required this.specialAttack,
    required this.specialDefense,
    required this.speed,
    required this.total,
    required this.pokemonTypes,
    required this.evolutions,
  });

  factory PokemonModel.fromJson(Map<String, dynamic> json) {
    return PokemonModel(
      id: json['id'] as String,
      name: json['name'] as String,
      height: json['height'] as String,
      weight: json['weight'] as String,
      imageUrl: json['imageurl'] as String,
      category: json['category'] as String,
      hp: json['hp'] as int,
      attack: json['attack'] as int,
      defense: json['defense'] as int,
      specialAttack: json['special_attack'] as int,
      specialDefense: json['special_defense'] as int,
      speed: json['speed'] as int,
      total: json['total'] as int,
      malePercentage: (json['male_percentage'] != null)
          ? json['male_percentage'] as String
          : '',
      femalePercentage: (json['female_percentage'] != null)
          ? json['female_percentage'] as String
          : '',
      pokemonTypes:
          (json['typeofpokemon'] as List).map((e) => e as String).toList(),
      evolutions: (json['evolutions'] != null)
          ? (json['evolutions'] as List).map((e) => e as String).toList()
          : List.empty(),
    );
  }
  getId(){
    return int.parse(id.substring(1,id.length));
  }
}
