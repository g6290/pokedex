import 'package:get/get.dart';
import 'package:pokedex/model/ability_model.dart';
import 'package:pokedex/repository/repository.dart';

class AbilityController extends GetxController {
  List<AbilityModel> listAbilityModel = <AbilityModel>[].obs;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    fetchListAbility();
  }

  fetchListAbility() async {
    listAbilityModel = await PokemonRepository().getAbility();
  }

}
