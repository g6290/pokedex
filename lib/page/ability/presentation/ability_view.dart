import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pokedex/page/ability/controller/ability_controller.dart';
import 'package:pokedex/routes/app_page.dart';
import 'package:pokedex/shared/config/app_type.dart';
import 'package:pokedex/shared/theme/style.dart';
import 'package:pokedex/widgets/custom_appbar.dart';

class AbilityView extends GetView<AbilityController> {
  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Scaffold(
        appBar: CustomAppBar(namePage: 'Ability'.tr),
        backgroundColor: Styles.whiteColor,
        body: _body(),
      );
    });
  }

  Widget _body() {
    return ListView.builder(
      padding: EdgeInsets.only(
        top: 12,
        left: 8,
        right: 12,
        bottom: 8,
      ),
      itemCount: controller.listAbilityModel.length,
      itemBuilder: (context, index) {
        return GestureDetector(
          child: FittedBox(
            fit: BoxFit.contain,
            child: Container(
              width: Get.width,
              margin: EdgeInsets.all(8),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  border: Border.all(color: Colors.blueAccent),
                  color: Styles.whiteColor,
                  ),
              child: Stack(
                children: [
                  Column(
                    children: [
                      SizedBox(
                        height: 36,
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 12),
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Generation Introduced: '.tr +
                              controller
                                  .listAbilityModel[index].generationIntroduced
                                  .toString(),
                          style: Get.theme.textTheme.bodyText1!
                              .copyWith(fontSize: 18),
                        ),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 12),
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Description: '.tr +
                              ' ' +
                              controller.listAbilityModel[index].description,
                          style: Get.theme.textTheme.bodyText2!
                              .copyWith(fontSize: 18),
                        ),
                      ),
                      SizedBox(
                        height: 14,
                      ),
                    ],
                  ),
                  Positioned(
                      left: 0,
                      top: 0,
                      child: Container(
                        height: 28,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20),
                              bottomRight: Radius.circular(12)),
                          color: AppType.generationIntroducedColor[controller
                              .listAbilityModel[index].generationIntroduced],
                        ),
                        child: FittedBox(
                          fit: BoxFit.contain,
                          child: Row(
                            children: [
                              SizedBox(
                                width: 8,
                              ),
                              Text(
                                '#' +
                                    controller.listAbilityModel[index].id
                                        .toString(),
                                style: Get.theme.textTheme.subtitle1!
                                    .copyWith(fontSize: 20, color: Styles.whiteColor),
                              ),
                              SizedBox(width: 8),
                              Text(
                                controller.listAbilityModel[index]
                                    .getNameFormat(controller
                                        .listAbilityModel[index].name),
                                style: Get.theme.textTheme.subtitle1!
                                    .copyWith(fontSize: 20, color: Styles.whiteColor),
                              ),
                              SizedBox(
                                width: 12,
                              ),
                            ],
                          ),
                        ),
                      )),
                ],
              ),
            ),
          ),
          onTap: () {
            Get.toNamed(Routes.abilityDetail,
                arguments: controller.listAbilityModel[index]);
          },
        );
      },
    );
  }
}
