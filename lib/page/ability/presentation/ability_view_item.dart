import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pokedex/model/ability_model.dart';
import 'package:pokedex/page/ability/controller/ability_controller.dart';
import 'package:pokedex/shared/theme/style.dart';

class AbilityViewItem extends GetView<AbilityController> {
  const AbilityViewItem({
    Key? key,
    required this.abilityModel,
    required this.onTap,
  }) : super(key: key);

  final AbilityModel abilityModel;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
            padding: EdgeInsets.all(16),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(16),
              boxShadow: [
                BoxShadow(
                  color: Styles.whiteColor!.withOpacity(0.7),
                  offset: Offset(0, 0),
                  blurRadius: 4,
                ),
              ],
            ),
            child:  Align(
              alignment: Alignment.topLeft,
              child: Padding(
                padding: const EdgeInsets.only(left: 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      abilityModel.name,
                      style: Get.theme.textTheme.headline5!.copyWith(
                          fontSize: 16
                      ),
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: abilityModel.pokemonFirstAbility.map((e) {
                        return Column(
                          children: [
                            Container(
                              padding: EdgeInsets.symmetric(vertical: 4, horizontal: 4),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                color: Colors.white38,),
                              child: Text(
                                e[0].toUpperCase() + e.substring(1, e.length),
                                style: Get.textTheme.headline6!.copyWith(
                                    color: Colors.black,
                                    fontSize:12
                                ),
                              ),
                            ),
                            SizedBox(height: 4,)
                          ],
                        );
                      }).toList(),
                    ),
                  ],
                ),
              ),
            ),
          ),

    );

  }
}