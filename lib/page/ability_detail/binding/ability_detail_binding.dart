import 'package:get/get.dart';
import 'package:pokedex/page/ability/controller/ability_controller.dart';
import 'package:pokedex/page/ability_detail/controller/ability_detail_controller.dart';

class AbilityDetailBinding extends Bindings{
  @override
  void dependencies() {
    // TODO: implement dependencies
    Get.put(AbilityDetailController());
  }
}