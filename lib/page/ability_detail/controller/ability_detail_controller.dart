import 'package:get/get.dart';
import 'package:pokedex/model/ability_model.dart';

class AbilityDetailController extends GetxController {
  Rx<AbilityModel> _rxAbilityModel = AbilityModel(
    id: 0,
    name: '',
    description: '',
    longDescription: '',
    generationIntroduced: '',
    pokemonFirstAbility: [],
    pokemonSecondAbility: [],
    pokemonHiddenAbility: [],
  ).obs;

  AbilityModel get abilityModel => _rxAbilityModel.value;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    _rxAbilityModel.value = Get.arguments;
  }
}