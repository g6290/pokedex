import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pokedex/page/ability_detail/controller/ability_detail_controller.dart';
import 'package:pokedex/shared/config/app_type.dart';
import 'package:pokedex/shared/theme/style.dart';

class AbilityDetail extends GetView<AbilityDetailController> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: DefaultTabController(
        length: 4,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: AppType.generationIntroducedColor[
                controller.abilityModel.generationIntroduced],
            leading: BackButton(
                color: Styles.whiteColor,
                onPressed: () {
                  Get.back();
                }),
            title: Text(
                controller.abilityModel
                    .getNameFormat(controller.abilityModel.name),
                style: Get.textTheme.headline1!
                    .copyWith(color: Styles.whiteColor, fontSize: 32)),
            bottom: const TabBar(
              tabs: [
                Tab(text: 'Information'),
                Tab(text: 'First Ability'),
                Tab(text: 'Second Ability'),
                Tab(text: 'Hidden Ability'),
              ],
            ),
          ),
          backgroundColor: Colors.white,
          body: _body(context),
        ),
      ),
    );
  }

  Widget _body(BuildContext context) {
    return TabBarView(
      children: [
        _buildInformation(),
        Container(
            child: _ability(
                listAbility: controller.abilityModel.pokemonFirstAbility)),
        Container(
            child: _ability(
                listAbility: controller.abilityModel.pokemonSecondAbility)),
        Container(
            child: _ability(
                listAbility: controller.abilityModel.pokemonHiddenAbility)),
      ],
    );
  }

  Widget _buildInformation() {
    return Container(
        height: 200,
        width: Get.width,
        child: Column(
          children: [
            SizedBox(height: 40),
            Padding(
              padding: EdgeInsets.all(16),
              child: Text(
                'Description: '.tr + controller.abilityModel.description,
                style: Get.textTheme.subtitle2!.copyWith(fontSize: 16),
              ),
            ),
            (controller.abilityModel.longDescription != '')
                ? (Padding(
                    padding: EdgeInsets.all(16),
                    child: Text(
                      ' ' + controller.abilityModel.longDescription,
                      style: Get.textTheme.subtitle2,
                    ),
                  ))
                : Container(),
            Text(
                'Generation Introduced: '.tr +
                    controller.abilityModel.getNameFormat(
                        controller.abilityModel.generationIntroduced),
                style: Get.textTheme.headline3)
          ],
        ));
  }

  Widget _ability({required List<String> listAbility}) {
    return Container(
      padding: EdgeInsets.only(top: 8, left: 16, right: 16),
      height: Get.height,
      width: Get.width,
      child: ListView.builder(
          itemCount: listAbility.length,
          itemBuilder: (BuildContext context, index) {
            return Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: AppType.generationIntroducedColor[
                      controller.abilityModel.generationIntroduced],
                ),
                margin: EdgeInsets.only(bottom: 8),
                height: 68.0,
                child: Center(
                    child: Text(
                  listAbility[index],
                  style: TextStyle(fontSize: 20, color: Styles.whiteColor),
                )));
          }),
    );
  }
}
