import 'package:get/get.dart';
import 'package:pokedex/page/ability/controller/ability_controller.dart';
import 'package:pokedex/page/dashboard/controller/dashboard_controller.dart';
import 'package:pokedex/page/move_view/controller/move_view_controller.dart';

class DashboardBinding extends Bindings {
  @override
  void dependencies() {
    // TODO: implement dependencies
    /// Dashboard dependencies
    Get.lazyPut<DashboardController>(() => DashboardController());
    Get.put(MoveViewController());
    Get.put(AbilityController());
  }
}
