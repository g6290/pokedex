import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pokedex/page/ability/presentation/ability_view.dart';
import 'package:pokedex/page/dashboard/controller/dashboard_controller.dart';
import 'package:pokedex/page/home_view/presentation/home_view.dart';
import 'package:pokedex/page/information/presentation/information_view.dart';
import 'package:pokedex/page/move_view/presentation/move_view.dart';


class DashboardPage extends GetView<DashboardController> {
  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Scaffold(
        body: IndexedStack(
          index: controller.tabIndex,
          children: [
            HomeView(),
            MoveView(),
            AbilityView(),
            InformationView(),
          ],
        ),
        bottomNavigationBar: BottomNavigationBar(
          unselectedItemColor: Colors.black,
          selectedItemColor: Colors.redAccent,
          onTap: controller.changeTabIndex,
          currentIndex: controller.tabIndex,
          showSelectedLabels: false,
          showUnselectedLabels: false,
          type: BottomNavigationBarType.fixed,
          backgroundColor: Colors.white,
          elevation: 0,
          items: [
            _bottomNavigationBarItem(
              icon: Icons.home,
              label: 'Home',
            ),
            _bottomNavigationBarItem(
              icon: Icons.new_releases_sharp,
              label: 'News',
            ),
            _bottomNavigationBarItem(
              icon: Icons.accessibility_new,
              label: 'Ability',
            ),
            _bottomNavigationBarItem(
              icon: Icons.person,
              label: 'Account',
            ),
          ],
        ),
      );
    });
  }

  _bottomNavigationBarItem({required IconData icon, required String label}) {
    return BottomNavigationBarItem(
      icon: Icon(icon),
      label: label,
    );
  }
}