import 'package:get/get.dart';
import 'package:pokedex/page/gen_view/controller/gen_view_controller.dart';

class GenViewBinding extends Bindings{
  @override
  void dependencies() {
    // TODO: implement dependencies
    Get.put(GenViewController());
  }
}