import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:pokedex/model/list_pokemon_model.dart';
import 'package:pokedex/model/gen_model.dart';
import 'package:pokedex/model/pokemon_model.dart';
import 'package:pokedex/repository/repository.dart';
import 'package:pokedex/shared/config/app_config.dart';
import 'package:pokedex/shared/theme/style.dart';

class GenViewController extends GetxController {
  Rx<ListPokemonModel> _rxListPokemonModel = ListPokemonModel(
    id: '',
    name: '',
    listPokemon: [],
  ).obs;

  ListPokemonModel get listPokemonModel => _rxListPokemonModel.value;

  Rx<GenModel> _rxGenModel =
      GenModel(id: '', name: '', color: Styles.whiteColor).obs;

  GenModel get genModel => _rxGenModel.value;

  RxBool _rxCheckType = false.obs;

  bool get checkType => _rxCheckType.value;

  RxBool _rxCheckTypeGrid = false.obs;

  bool get checkTypeGrid => _rxCheckTypeGrid.value;

  RxDouble _rxPaddingGrid = 12.0.obs;

  double get paddingGrid => _rxPaddingGrid.value;

  RxDouble _rxImageSize = ((Get.width-64)*0.224).obs;

  double get imageSize => _rxImageSize.value;

  RxDouble _rxTextSize = 20.0.obs;

  double get textSize => _rxTextSize.value;

  RxDouble _rxTextTypeSize = 12.0.obs;

  double get textTypeSize => _rxTextTypeSize.value;

  RxInt _rxCountGrid = 2.obs;

  int get countGrid => _rxCountGrid.value;

  RxDouble _rxRadioGrid = 1.4.obs;

  double get radioGrid => _rxRadioGrid.value;

  TextEditingController searchController = TextEditingController();

  RxList<PokemonModel> resultSearch = <PokemonModel>[].obs;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    _rxGenModel.value = Get.arguments;
    fetchPokemonArea();
  }

  fetchPokemonArea() async {
    _rxListPokemonModel.value =
        await PokemonRepository().getPokemonGen(genId: _rxGenModel.value.id);
  }

  changeType() {
    _rxCheckType.value = !_rxCheckType.value;
  }

  changeGrid() {
    _rxCheckTypeGrid.value = !_rxCheckTypeGrid.value;
  }

  searchPokemon(String searchKey) {
    if (searchKey == '') {
      resultSearch.clear();
    } else {
      resultSearch.clear();
      String key = searchKey.toLowerCase();
      _rxListPokemonModel.value.listPokemon.forEach((pokemonModel) {
        String namePokemon = pokemonModel.name.toLowerCase();
        if (namePokemon.contains(key)) {
          resultSearch.add(pokemonModel);
        }
      });
    }
  }

  changeGridLayout({required int type}) {
    switch (type) {
      case AppConfig.gridViewSecond:
        _rxPaddingGrid.value = 12;
        _rxImageSize.value = ((Get.width-64)*0.224);
        _rxTextSize.value = 20;
        _rxCountGrid.value = 2;
        _rxRadioGrid.value = 1.4;
        _rxTextTypeSize.value = 12;
        _rxCheckTypeGrid.value = !_rxCheckTypeGrid.value;
        break;
      case AppConfig.gridViewThird:
        _rxPaddingGrid.value = 12;
        _rxImageSize.value = ((Get.width-80)*0.14);
        _rxTextSize.value = 14;
        _rxCountGrid.value = 3;
        _rxRadioGrid.value = 1.2;
        _rxTextTypeSize.value = 8;
        _rxCheckTypeGrid.value = !_rxCheckTypeGrid.value;
        break;
      case AppConfig.gridViewFourth:
        _rxPaddingGrid.value = 8;
        _rxImageSize.value = ((Get.width-96)*0.14);
        _rxTextSize.value = 10;
        _rxCountGrid.value = 4;
        _rxRadioGrid.value = 1;
        _rxCheckTypeGrid.value = !_rxCheckTypeGrid.value;
        break;
      default:
        _rxPaddingGrid.value = 12;
        _rxImageSize.value = 80;
        _rxTextSize.value = 20;
        _rxCountGrid.value = 2;
        _rxRadioGrid.value = 1.4;
        _rxTextTypeSize.value = 12;
        _rxCheckTypeGrid.value = !_rxCheckTypeGrid.value;
        break;
    }
  }
}
