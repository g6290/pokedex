import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:pokedex/page/gen_view/controller/gen_view_controller.dart';
import 'package:pokedex/page/gen_view/presentation/gen_view_item.dart';
import 'package:pokedex/routes/app_page.dart';
import 'package:pokedex/shared/config/app_config.dart';
import 'package:pokedex/shared/config/app_type.dart';
import 'package:pokedex/shared/theme/style.dart';

class GenView extends GetView<GenViewController> {
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Scaffold(
        appBar: AppBar(
          leading: BackButton(),
          flexibleSpace: Container(
            alignment: Alignment.centerLeft,
            height: Get.height,
            padding: EdgeInsets.only(left: 48),
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.topRight,
                colors: [Colors.lightBlueAccent, Colors.greenAccent],
              ),
            ),
          ),
          title: Text(controller.genModel.name),
          actions: [
            IconButton(
              icon: Icon(Icons.search),
              onPressed: () {
                controller.resultSearch.clear();
                Get.toNamed(Routes.search);
              },
            ),
            (controller.checkType)
                ? IconButton(
                    icon: Icon(Icons.grid_view),
                    onPressed: () {
                      controller.changeType();
                    })
                : IconButton(
                    icon: Icon(Icons.list),
                    onPressed: () {
                      controller.changeType();
                    }),
          ],
        ),
        backgroundColor: Colors.white,
        floatingActionButton: (!controller.checkType)
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  (controller.checkTypeGrid)
                      ? GestureDetector(
                          onTap: () {
                            controller.changeGridLayout(type: AppConfig.gridViewFourth);
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Container(
                                alignment: Alignment.center,
                                height: 40,
                                width: 100,
                                decoration: BoxDecoration(
                                    color: Colors.blue,
                                    borderRadius: BorderRadius.circular(16)),
                                child: Text('Grid 4',
                                    style: Get.textTheme.headline3!.copyWith(
                                        fontWeight: FontWeight.normal,
                                        color: Styles.whiteColor)),
                              ),
                              SizedBox(width: 8),
                              Container(
                                height: 40,
                                width: 40,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(100),
                                  color: Colors.blue,
                                ),
                                child: Icon(
                                  Icons.fact_check_rounded,
                                  color: Styles.whiteColor,
                                ),
                              ),
                              SizedBox(width: 8)
                            ],
                          ),
                        )
                      : Container(),
                  SizedBox(height: 8),
                  (controller.checkTypeGrid)
                      ? GestureDetector(
                          onTap: () {
                            controller.changeGridLayout(type: AppConfig.gridViewThird);
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Container(
                                alignment: Alignment.center,
                                height: 40,
                                width: 100,
                                decoration: BoxDecoration(
                                    color: Colors.blue,
                                    borderRadius: BorderRadius.circular(16)),
                                child: Text('Grid 3',
                                    style: Get.textTheme.headline3!.copyWith(
                                        fontWeight: FontWeight.normal,
                                        color: Styles.whiteColor)),
                              ),
                              SizedBox(width: 8),
                              Container(
                                height: 40,
                                width: 40,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(100),
                                  color: Colors.blue,
                                ),
                                child: Icon(
                                  Icons.fact_check_rounded,
                                  color: Styles.whiteColor,
                                ),
                              ),
                              SizedBox(width: 8)
                            ],
                          ),
                        )
                      : Container(),
                  SizedBox(height: 8),
                  (controller.checkTypeGrid)
                      ? GestureDetector(
                          onTap: () {
                            //controller.changeGrid2();
                            controller.changeGridLayout(type: AppConfig.gridViewSecond);
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Container(
                                alignment: Alignment.center,
                                height: 40,
                                width: 100,
                                decoration: BoxDecoration(
                                    color: Colors.blue,
                                    borderRadius: BorderRadius.circular(16)),
                                child: Text('Grid 2',
                                    style: Get.textTheme.headline3!.copyWith(
                                        fontWeight: FontWeight.normal,
                                        color: Styles.whiteColor)),
                              ),
                              SizedBox(width: 8),
                              Container(
                                height: 40,
                                width: 40,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(100),
                                  color: Colors.blue,
                                ),
                                child: Icon(
                                  Icons.fact_check_rounded,
                                  color: Styles.whiteColor,
                                ),
                              ),
                              SizedBox(width: 8)
                            ],
                          ),
                        )
                      : Container(),
                  SizedBox(height: 8),
                  FloatingActionButton(
                      child: (controller.checkTypeGrid)
                          ? Icon(Icons.cancel)
                          : Icon(Icons.menu),
                      onPressed: () {
                        controller.changeGrid();
                      })
                ],
              )
            : Container(),
        body: Obx(()=> (controller.checkType) ? _buildListView() : _buildGridView()),
      ),
    );
  }

  Widget _buildListView() {
    return ListView.builder(
      padding: EdgeInsets.all(16),
      itemCount: controller.listPokemonModel.listPokemon.length,
      itemBuilder: (context, index) {
        return Container(
          height: 72,
          alignment: Alignment.center,
          margin: EdgeInsets.only(bottom: 8),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16),
            color: AppType.typePokemonColor[controller.listPokemonModel.listPokemon[index].pokemonTypes[0]]
          ),
          child: ListTile(
            leading: Hero(
              tag: controller.listPokemonModel.listPokemon[index].imageUrl,
              child: Image.network(
                  controller.listPokemonModel.listPokemon[index].imageUrl),
            ),
            title: Text(
              controller.listPokemonModel.listPokemon[index].name,
              style: Get.theme.textTheme.subtitle1!
                  .copyWith(fontSize: 24, color: Colors.black54),
            ),
            onTap: () {
              Get.toNamed(Routes.pokemonview,arguments:  controller.listPokemonModel.listPokemon[index]);
            },
          ),
        );
      },
    );
  }

  Widget _buildGridView() {
    return GridView.count(
      shrinkWrap: true,
      primary: false,
      crossAxisCount: controller.countGrid,
      padding: EdgeInsets.all(24),
      childAspectRatio: controller.radioGrid,
      mainAxisSpacing: 16.0,
      crossAxisSpacing: 16.0,
      children: controller.listPokemonModel.listPokemon
          .map((pokemonModel) => GenViewItem(
                pokemonModel: pokemonModel,
                imageSize: controller.imageSize,
                fontSize: controller.textSize,
                padding: controller.paddingGrid,
                textTypeSize: controller.textTypeSize,
                onTap: () {
                  Get.toNamed(Routes.pokemonview,arguments: pokemonModel);
                },
              ))
          .toList(),
    );
  }
}
