import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pokedex/model/pokemon_model.dart';
import 'package:pokedex/page/gen_view/controller/gen_view_controller.dart';
import 'package:pokedex/shared/config/app_type.dart';
import 'package:pokedex/shared/theme/style.dart';

class GenViewItem extends GetView<GenViewController> {
  const GenViewItem({
    Key? key,
    required this.pokemonModel,
    this.padding = 12,
    this.imageSize = 88,
    this.fontSize = 16,
    this.textTypeSize = 12,
    required this.onTap,
  }) : super(key: key);

  final PokemonModel pokemonModel;
  final double fontSize;
  final double imageSize;
  final double padding;
  final double textTypeSize;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Stack(
        children: [
          Container(
            width: (Get.width - 64) * .5,
            padding: EdgeInsets.all(padding),
            decoration: BoxDecoration(
              color: AppType.typePokemonColor[pokemonModel.pokemonTypes[0]],
              borderRadius: BorderRadius.circular(16),
              boxShadow: [
                BoxShadow(
                  color: AppType.typePokemonColor[pokemonModel.pokemonTypes[0]]!
                      .withOpacity(0.7),
                  spreadRadius: -8,
                  offset: Offset(0, 16),
                  blurRadius: 4,
                ),
              ],
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                (controller.countGrid != 4)
                    ?Text(
                  pokemonModel.name,
                  style: Get.textTheme.headline6!
                      .copyWith(fontSize: fontSize, color: Styles.whiteColor),
                )
                :Center(
                  child: Text(
                    pokemonModel.name,
                    style: Get.textTheme.headline6!
                        .copyWith(fontSize: fontSize, color: Styles.whiteColor),
                  ),
                ),
                SizedBox(height: 8),
                (controller.countGrid != 4)
                    ? Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: pokemonModel.pokemonTypes.map((e) {
                          return _typePokemon(
                            typePokemon: e,
                            colorType: AppType
                                .typePokemonColor[pokemonModel.pokemonTypes[0]],
                          );
                        }).toList(),
                      )
                    : Container(),
              ],
            ),
          ),
          (controller.countGrid != 4)
              ? Positioned(
                  bottom: 8,
                  right: 8,
                  child: Hero(
                    tag: pokemonModel.imageUrl,
                    child: CachedNetworkImage(
                      imageUrl: pokemonModel.imageUrl,
                      errorWidget: (context, url, error) => Icon(Icons.error),
                      height: imageSize,
                    ),
                  ),
                )
              : Positioned(
                  bottom: 8,
                  right: 16,
                  child: Hero(
                    tag: pokemonModel.imageUrl,
                    child: CachedNetworkImage(
                      imageUrl: pokemonModel.imageUrl,
                      errorWidget: (context, url, error) => Icon(Icons.error),
                      height: imageSize,
                    ),
                  ),
                )
        ],
      ),
    );
  }

  Widget _typePokemon(
      {required String typePokemon, required Color? colorType}) {
    return Container(
      margin: EdgeInsets.only(bottom: 4),
      padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8), color: Styles.whiteColor),
      child: Text(
        typePokemon,
        style: Get.theme.textTheme.headline6!.copyWith(
            fontSize: textTypeSize,
            color: colorType,
            fontWeight: FontWeight.bold),
      ),
    );
  }
}
