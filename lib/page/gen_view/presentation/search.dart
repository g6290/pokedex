import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:pokedex/page/gen_view/controller/gen_view_controller.dart';
import 'package:pokedex/page/gen_view/presentation/gen_view_item.dart';
import 'package:pokedex/routes/app_page.dart';
import 'package:pokedex/shared/theme/style.dart';

class Search extends GetView<GenViewController> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Obx(
          () => Scaffold(
        appBar: AppBar(
          leading: BackButton(),
          flexibleSpace: Container(
            alignment: Alignment.centerLeft,
            height: Get.height,
            padding: EdgeInsets.only(left: 48),
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.topRight,
                colors: [Colors.lightBlueAccent, Colors.greenAccent],
              ),
            ),
          ),
          titleSpacing: 0,
          title: Container(
            height: 32,
            padding: EdgeInsets.symmetric(
              horizontal: 12,
              vertical: 4,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16),
              color: Styles.whiteColor,
            ),
            child: TextField(
              keyboardType: TextInputType.text,
              cursorColor: Styles.blackColor,
              style: TextStyle(
                fontSize: 20,
                color: Styles.blackColor,
                decorationThickness: 0.0,
              ),
              controller: controller.searchController,
              decoration: InputDecoration.collapsed(
                hintText: 'Name Pokemon',
              ),
              onChanged: (text) {
                controller.searchPokemon(text);
              },
            ),
          ),
          actions: [
            (controller.checkType)
                ? IconButton(
                icon: Icon(Icons.grid_view),
                onPressed: () {
                  controller.changeType();
                })
                : IconButton(
                icon: Icon(Icons.list),
                onPressed: () {
                  controller.changeType();
                })
          ],
        ),
        body: (controller.checkType) ? _buildListView() : _buildGridView(),
      ),
    );
  }

  Widget _buildListView() {
    return ListView.builder(
      padding: EdgeInsets.all(16),
      itemCount: controller.resultSearch.length,
      itemBuilder: (context, index) {
        return Column(
          children: [
            ListTile(
              leading: Hero(
                tag: controller.resultSearch[index].imageUrl,
                child: Image.network(
                    controller.resultSearch[index].imageUrl),
              ),
              title: Text(
                controller.resultSearch[index].name,
                style: Get.theme.textTheme.subtitle1!
                    .copyWith(fontSize: 24, color: Colors.black54),
              ),
              onTap: () {
                Get.toNamed(Routes.pokemonview,arguments: controller.resultSearch[index]);
              },
            ),
            Divider(
              thickness: 2,
            ),
          ],
        );
      },
    );
  }

  Widget _buildGridView() {
    return GridView.count(
      shrinkWrap: true,
      primary: false,
      crossAxisCount: 2,
      padding: EdgeInsets.all(24),
      childAspectRatio: 1.4,
      mainAxisSpacing: 16.0,
      crossAxisSpacing: 16.0,
      children: controller.resultSearch.map((pokemonModel) {
        return GenViewItem(pokemonModel: pokemonModel, onTap: (){
          Get.toNamed(Routes.pokemonview,arguments: pokemonModel);
        });
      }).toList(),
    );
  }
}
