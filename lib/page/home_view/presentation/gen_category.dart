import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pokedex/model/gen_model.dart';
import 'package:pokedex/shared/config/app_image.dart';
import 'package:pokedex/shared/theme/style.dart';

class GenCategory extends StatelessWidget {
  GenCategory({
    required this.genModel,
    required this.onTap,
  });

  final GenModel genModel;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          color: genModel.color,
          borderRadius: BorderRadius.circular(16),
          boxShadow: [
            BoxShadow(
                color: genModel.color.withOpacity(0.6),
                offset: Offset(0, 20),
                blurRadius: 4,
                spreadRadius: -9
            ),
          ],
        ),
        child: Stack(
          children: [
            _buildPokemonDecoration(height: 64),
            _buildCircleDecoration(height: 64),
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.only(left: 12),
                child: Text(
                  genModel.name,
                  style: Get.theme.textTheme.headline5!.copyWith(
                    color: Styles.textColorWhite,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }


  Widget _buildCircleDecoration({required double height}) {
    return Positioned(
      top: -height * 0.6,
      left: -height * 0.54,
      child: CircleAvatar(
        radius: (height * 1.03) / 2,
        backgroundColor: Colors.white.withOpacity(0.14),
      ),
    );
  }

  Widget _buildPokemonDecoration({required double height}) {
    return Positioned(
      top: -height * 0.12,
      right: -height * 0.24,
      child: Image(
        image: AppImage.pokeBall,
        width: height * 1.48,
        color: Colors.white.withOpacity(0.14),
      ),
    );
  }
}
