import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pokedex/routes/app_page.dart';
import 'package:pokedex/shared/config/app_type.dart';
import 'package:pokedex/widgets/custom_appbar.dart';

import 'gen_category.dart';


class HomeView extends GetView {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 32),
            Padding(
                padding: EdgeInsets.only(right: 16, left: 16),
                child: Text(
                  'What Pokemon\nare you looking for?',
                  style: Get.theme.textTheme.headline1!.copyWith(fontSize: 36),
                )),
            const SizedBox(height: 32),

            _buildListArea(),
          ],
        ),
      ),
    );
  }

  Widget _buildListArea() {
    return GridView.count(
      shrinkWrap: true,
      primary: false,
      crossAxisCount: 2,
      padding: EdgeInsets.all(16),
      childAspectRatio: 2.4,
      mainAxisSpacing: 24.0,
      crossAxisSpacing: 8.0,
      children: AppType.genList.map((genModel) {
        return GenCategory(
          genModel: genModel,
          onTap: () {
            Get.toNamed(Routes.genview, arguments: genModel);
          },
        );
      }).toList(),
    );
  }
}
