import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pokedex/shared/config/app_image.dart';
import 'package:pokedex/shared/theme/style.dart';
import 'package:pokedex/widgets/custom_appbar.dart';
import 'package:url_launcher/url_launcher.dart';

class InformationView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: CustomAppBar(namePage: 'Information'.tr),
      body: _body(),
    );
  }

  Widget _body() {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.topRight,
          colors: [Colors.lightBlueAccent, Colors.greenAccent],
        ),
      ),
      child: Stack(
        children: [
          Container(
            width: Get.width,
            height: Get.height,
            margin: EdgeInsets.only(top: 128),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(36), topLeft: Radius.circular(36)),
              color: Styles.whiteColor,
            ),
            child: Padding(
              padding: EdgeInsets.only(
                left: 12,
                right: 4,
              ),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Center(
                      child: Text('Pokedex',
                          style: Get.theme.textTheme.headline1!
                              .copyWith(fontSize: 24)),
                    ),
                    SizedBox(height: 20),
                    Text(
                      ' An app from scratch shows Pokémon information with pictures and skills using GetX state management.',
                      style: Get.textTheme.subtitle1!.copyWith(
                        color: Colors.black,
                        fontSize: 16,
                      ),
                    ),
                    SizedBox(height: 24),
                    Text(
                      'Member: ',
                      style: Get.textTheme.headline2!.copyWith(
                        color: Colors.black,
                        fontSize: 16,
                      ),
                    ),
                    SizedBox(height: 12),
                    Text(
                      '- Phạm Văn Hào Quang ',
                      style: Get.textTheme.subtitle1!.copyWith(
                        color: Colors.black,
                        fontSize: 16,
                      ),
                    ),
                    SizedBox(height: 8),
                    Text(
                      '- Trần Hữu Phát',
                      style: Get.textTheme.subtitle1!.copyWith(
                        color: Colors.black,
                        fontSize: 16,
                      ),
                    ),
                    SizedBox(height: 8),
                    Text(
                      '- Nguyễn Trần Xuân Hải',
                      style: Get.textTheme.subtitle1!.copyWith(
                        color: Colors.black,
                        fontSize: 16,
                      ),
                    ),
                    SizedBox(height: 24),
                    Text(
                      'References:',
                      style: Get.textTheme.headline2!.copyWith(
                        color: Colors.black,
                        fontSize: 16,
                      ),
                    ),
                    SizedBox(height: 16),
                    Text(
                      'API:',
                      style: Get.textTheme.headline3!.copyWith(
                        color: Colors.black,
                        fontSize: 16,
                      ),
                    ),
                    SizedBox(height: 8),
                    Text(
                      'https://pokeapi.co/docs/v2',
                      style: Get.textTheme.subtitle1!.copyWith(
                        color: Colors.black,
                        fontSize: 16,
                      ),
                    ),
                    SizedBox(height: 16),
                    Text(
                      'UI/UX:',
                      style: Get.textTheme.headline3!.copyWith(
                        color: Colors.black,
                        fontSize: 16,
                      ),
                    ),
                    SizedBox(height: 8),
                    RichText(
                      text: TextSpan(
                          text: 'Dribbble',
                          style: Get.textTheme.subtitle1!.copyWith(
                            color: Colors.black,
                            fontSize: 16,
                          ),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              launch(
                                  'https://dribbble.com/shots/6545819-Pokedex-App?utm_source=pinterest&utm_campaign=pinterest_shot&utm_content=Pokedex+App&utm_medium=Social_Share');
                            }),
                    ),
                    SizedBox(height: 24),
                  ],
                ),
              ),
            ),
          ),
          Positioned(
            top: 20,
            left: 100,
            child: Image(image: AppImage.pokedex),
            height: 88,
          ),
        ],
      ),
    );
  }
}
