import 'package:get/get.dart';
import 'package:pokedex/page/move_detail/controller/move_detail_controller.dart';

class MoveDetailBinding extends Bindings{
  @override
  void dependencies() {
    // TODO: implement dependencies
    Get.put(MoveDetailController());
  }
}