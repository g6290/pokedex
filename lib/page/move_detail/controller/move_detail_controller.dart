import 'package:get/get.dart';
import 'package:pokedex/model/move_model.dart';

class MoveDetailController extends GetxController {
  Rx<MoveModel> _rxMoveModel = MoveModel(
    name: '',
    type: '',
    category: '',
    power: 0,
    accuracy: 0,
    pp: 0,
  ).obs;

  MoveModel get moveModel => _rxMoveModel.value;
  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    _rxMoveModel.value = Get.arguments;
  }
}
