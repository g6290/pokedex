import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:pokedex/page/move_detail/controller/move_detail_controller.dart';
import 'package:pokedex/shared/config/app_image.dart';
import 'package:pokedex/shared/config/app_type.dart';
import 'package:pokedex/shared/theme/style.dart';
import 'package:pokedex/widgets/vertial_devider.dart';

class MoveDetail extends GetView<MoveDetailController> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Obx(
      () => Scaffold(
        backgroundColor:
            AppType.typePokemonColor[controller.moveModel.formatType()],
        body: _body(),
      ),
    );
  }

  Widget _body() {
    return Stack(
      children: [
        Container(
          width: Get.width,
          height: Get.height,
          margin: EdgeInsets.only(top: 160),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(56), topLeft: Radius.circular(56)),
            color: Styles.whiteColor,
          ),
          child: Padding(
            padding: EdgeInsets.only(left: 24,right: 24,),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(
                    height: 84,
                  ),
                  Text(
                    controller.moveModel.formatName(),
                    style: Get.textTheme.headline2!.copyWith(
                      fontWeight: FontWeight.w300,
                      fontSize: 48,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: AppType.typePokemonColor[controller.moveModel.formatType()],
                    ),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Image(
                          image: AppImage.findImageType(controller.moveModel.type)!,
                          height: 24,
                        ),
                        SizedBox(width: 12),
                        Text(controller.moveModel.type.toUpperCase(),style: Get.textTheme.headline6!.copyWith(color: Styles.whiteColor),),
                      ],
                    ),
                  ),
                  SizedBox(height: 24),
                  Text('Absorb deals damage and the user will recover 50% of the HP drained.',style: Get.textTheme.subtitle1!.copyWith(color: Colors.grey,fontSize: 16,),),
                  SizedBox(height: 24),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      _detailItem(key: 'Power'.tr, data: controller.moveModel.power, color: AppType.typePokemonColor[controller.moveModel.formatType()]),
                      CustomVerticalDivider(color:Colors.black12,height: 52,),
                      _detailItem(key: 'Accuracy'.tr, data: controller.moveModel.accuracy, color: AppType.typePokemonColor[controller.moveModel.formatType()]),
                      CustomVerticalDivider(color:Colors.black12,height: 52,),
                      _detailItem(key: 'PP'.tr, data: controller.moveModel.pp, color: AppType.typePokemonColor[controller.moveModel.formatType()]),
                    ],
                  ),
                  SizedBox(height: 24),
                  _detailItem(key: 'Category', data: controller.moveModel.formatCategory(),color: AppType.typePokemonColor[controller.moveModel.formatType()]),
                ],
              ),
            ),
          ),
        ),
        Positioned(
            top: 110,
            left: Get.width / 2 - 50,
            child: Image(
              image: AppImage.findImageType(controller.moveModel.type)!,
              height: 100,
            )),
        Positioned(
            top: 32,
            left: 12,
            child: BackButton(color: Styles.whiteColor)),
      ],
    );
  }
  Widget _detailItem({required String key,required dynamic data,required Color? color}){
    return Container(
      width: (Get.width-68)/3,
      child: Column(
        children: [
          Text(key,style: Get.textTheme.headline1!.copyWith(color: color,fontWeight: FontWeight.normal),),
          SizedBox(
            height: 8,
          ),
          (data==0)
              ? Icon(Icons.cancel_outlined,color: Colors.grey)
              : Text('$data',style: Get.textTheme.subtitle1!.copyWith(color: Colors.grey,fontSize: 20,),),
        ],
      ),
    );
  }
}
