import 'package:get/get.dart';
import 'package:pokedex/model/move_model.dart';
import 'package:pokedex/repository/repository.dart';

class MoveViewController extends GetxController{
  List<MoveModel> listMoves= <MoveModel>[].obs;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    fetchMoves();
  }
  fetchMoves() async {
    listMoves = await PokemonRepository().getMoves();
  }
}