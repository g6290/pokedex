import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:pokedex/page/move_view/controller/move_view_controller.dart';
import 'package:pokedex/routes/app_page.dart';
import 'package:pokedex/shared/config/app_image.dart';
import 'package:pokedex/shared/helper/helper.dart';
import 'package:pokedex/shared/theme/style.dart';
import 'package:pokedex/widgets/custom_appbar.dart';

class MoveView extends GetView<MoveViewController> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: CustomAppBar(namePage: 'Moves'.tr),
      backgroundColor: Styles.whiteColor,
      body: _buildListMoves(),
    );
  }
  Widget _buildListMoves() {
    return ListView.builder(
      shrinkWrap: true,
      primary: false,
      itemCount: controller.listMoves.length,
      itemBuilder: (context, index) {
        return Padding(
          padding: EdgeInsets.only(left: 24,right: 24,),
          child: Column(
            children: [
              ListTile(
                onTap: () {
                  Get.toNamed(Routes.movedetail,
                      arguments: controller.listMoves[index]);
                },
                title: Text(
                  controller.listMoves[index].formatName(),
                  style: Get.textTheme.subtitle1!
                      .copyWith(fontSize: 24, fontWeight: FontWeight.normal),
                ),
                trailing: Image(
                  image: AppImage.findImageType(controller.listMoves[index].type)!,
                  height: 40,
                ),
              ),
              Divider(
                thickness: 2,
              ),
            ],
          ),
        );
      },
    );
  }
}
