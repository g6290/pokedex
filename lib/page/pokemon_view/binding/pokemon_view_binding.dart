import 'package:get/get.dart';
import 'package:pokedex/page/pokemon_view/controller/pokemon_view_controller.dart';

class PokemonViewBinding extends Bindings{
  @override
  void dependencies() {
    // TODO: implement dependencies
    Get.put(PokemonViewController());
  }
}