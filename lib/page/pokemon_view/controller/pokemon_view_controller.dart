import 'package:get/get.dart';
import 'package:pokedex/model/pokemon_model.dart';
import 'package:pokedex/repository/repository.dart';

class PokemonViewController extends GetxController {
  Rx<PokemonModel> _rxPokemonModel = PokemonModel(
    id: '',
    name: '',
    height: '',
    weight: '',
    imageUrl: '',
    category: '',
    malePercentage: '',
    femalePercentage: '',
    hp: 0,
    attack: 0,
    defense: 0,
    specialAttack: 0,
    specialDefense: 0,
    speed: 0,
    total: 0,
    pokemonTypes: [],
    evolutions: [],
  ).obs;

  PokemonModel get pokemonModel => _rxPokemonModel.value;

  List<String> listEvolution = [];

  RxBool _rxIsLoading = false.obs;

  bool get isLoading => _rxIsLoading.value;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    _rxPokemonModel.value = Get.arguments;
    fetchData();
  }

  fetchData() async {
    _rxIsLoading.value = true;
    await fetch();
    _rxIsLoading.value = false;
  }

  String getId(String value) => int.parse(value.substring(1, value.length)).toString();

  fetch() async {
    listEvolution.clear();
    List<String> listId = [];
    _rxPokemonModel.value.evolutions.forEach((evolution) => listId.add(getId(evolution)));
    listEvolution = await PokemonRepository.getEvolutionName(listId: listId);
  }
}
