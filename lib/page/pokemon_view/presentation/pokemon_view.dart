import 'package:cached_network_image/cached_network_image.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:pokedex/page/pokemon_view/controller/pokemon_view_controller.dart';
import 'package:pokedex/shared/config/app_type.dart';
import 'package:pokedex/shared/theme/style.dart';
import 'package:pokedex/widgets/base_stats.dart';
import 'package:pokedex/widgets/vertial_devider.dart';

class PokemonView extends GetView<PokemonViewController> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.white,
      body: _body(context),
    );
  }

  Widget _body(BuildContext context) {
    Color? colorPokemon =
        AppType.typePokemonColor[controller.pokemonModel.pokemonTypes[0]];
    return SingleChildScrollView(
      child: Column(
        children: [
          _buildHeader(context),
          SizedBox(height: 16),
          _buildType(),
          SizedBox(height: 16),
          _buildDetailData(),
          SizedBox(height: 24),
          Text(
            'Evolution',
            style: Get.textTheme.headline2!.copyWith(color: colorPokemon),
          ),
          Obx(
                () => (controller.isLoading)
                ? CircularProgressIndicator()
                : _buildEvolution(),
          ),
          _buildBaseStats(),
          SizedBox(
            height: 32,
          )
        ],
      ),
    );
  }
  Widget _buildHeader(BuildContext context){
    Color? colorPokemon =
    AppType.typePokemonColor[controller.pokemonModel.pokemonTypes[0]];
    return Container(
      height: 340,
      child: Stack(
        children: [
          ClipPath(
            clipper: MyCusTomClipper(),
            child: Container(
                height: 320,
                color: colorPokemon,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 32),
                    BackButton(
                      color: Colors.white,
                      onPressed: () {
                        Navigator.popUntil(
                            context, ModalRoute.withName('/genview'));
                      },
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 24, left: 24),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            controller.pokemonModel.name,
                            style: Get.textTheme.headline1!.copyWith(
                                color: Styles.textColorWhite,
                                fontSize: 32),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 8),
                            child: Text(
                              controller.pokemonModel.id,
                              style: Get.textTheme.headline2!
                                  .copyWith(color: Styles.textColorWhite),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                )),
          ),
          Positioned(
            left: Get.width / 4,
            top: 128,
            child: Hero(
              tag: controller.pokemonModel.imageUrl,
              child: CachedNetworkImage(
                imageUrl: controller.pokemonModel.imageUrl,
                errorWidget: (context, url, error) => Icon(Icons.error),
                height: 220,
              ),
            ),),
        ],
      ),
    );
  }
  Widget _buildType(){
    Color? colorPokemon =
    AppType.typePokemonColor[controller.pokemonModel.pokemonTypes[0]];
    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: controller.pokemonModel.pokemonTypes.map((type) {
          return GestureDetector(
            onTap: () {},
            child: Container(
              alignment: Alignment.center,
              height: 32,
              width: (Get.width - 96) / 2,
              decoration: BoxDecoration(
                color: colorPokemon,
                borderRadius: BorderRadius.circular(20),
              ),
              child: Text(
                type,
                style: Get.textTheme.headline3!
                    .copyWith(color: Styles.textColorWhite),
              ),
            ),
          );
        }).toList());
  }
  Widget _buildDetailData(){
    Color? colorPokemon =
    AppType.typePokemonColor[controller.pokemonModel.pokemonTypes[0]];
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.only(left: 24, right: 24),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                width: (Get.width - 68) / 3 + 8,
                child: Column(
                  children: [
                    Text(
                      controller.pokemonModel.category,
                      style: Get.textTheme.headline2!.copyWith(
                        color: colorPokemon,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                    Text(
                      'Category',
                      style: Get.textTheme.headline6!.copyWith(
                          fontWeight: FontWeight.normal, color: Colors.grey),
                    ),
                  ],
                ),
              ),
              CustomVerticalDivider(
                height: 80,
                color: colorPokemon,
                indent: 10,
                endIndent: 10,
              ),
              Container(
                width: (Get.width - 68) / 3 - 16,
                child: Column(
                  children: [
                    Text(
                      controller.pokemonModel.height,
                      style: Get.textTheme.headline2!.copyWith(
                        color: colorPokemon,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                    Text(
                      'Heights',
                      style: Get.textTheme.headline6!.copyWith(
                          fontWeight: FontWeight.normal, color: Colors.grey),
                    ),
                  ],
                ),
              ),
              CustomVerticalDivider(
                height: 80,
                color: colorPokemon,
                indent: 10,
                endIndent: 10,
              ),
              Container(
                width: (Get.width - 68) / 3 + 8,
                child: Column(
                  children: [
                    Text(
                      controller.pokemonModel.weight,
                      style: Get.textTheme.headline2!.copyWith(
                        color: colorPokemon,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                    Text(
                      'Weights',
                      style: Get.textTheme.headline6!.copyWith(
                          fontWeight: FontWeight.normal, color: Colors.grey),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 12),
        Padding(
          padding: EdgeInsets.only(left: 24, right: 24),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Column(
                children: [
                  Text(
                    'Male',
                    style: Get.textTheme.headline2!.copyWith(
                      color: colorPokemon,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                  Text(
                    controller.pokemonModel.malePercentage,
                    style: Get.textTheme.headline6!.copyWith(
                        fontWeight: FontWeight.normal, color: Colors.grey),
                  ),
                ],
              ),
              Column(
                children: [
                  Text(
                    'Female',
                    style: Get.textTheme.headline2!.copyWith(
                      color: colorPokemon,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                  Text(
                    controller.pokemonModel.femalePercentage,
                    style: Get.textTheme.headline6!.copyWith(
                        fontWeight: FontWeight.normal, color: Colors.grey),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
  Widget _buildEvolution(){
    Color? colorPokemon =
    AppType.typePokemonColor[controller.pokemonModel.pokemonTypes[0]];
    return ListView.builder(
      shrinkWrap: true,
      primary: false,
      itemCount: controller.listEvolution.length-1,
      itemBuilder: (BuildContext context,index) {
        return Padding(
          padding: EdgeInsets.only(left: 24,right: 24,bottom: 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Column(
                children: [
                  CachedNetworkImage(
                    imageUrl: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/${controller.pokemonModel.evolutions[index].substring(1,controller.pokemonModel.evolutions[index].length)}.png',
                    errorWidget: (context, url, error) => Icon(Icons.error),
                    height: 100,
                  ),
                  Text(controller.listEvolution[index][0].toUpperCase()+controller.listEvolution[index].substring(1,controller.listEvolution[index].length),style: TextStyle(color: colorPokemon,fontSize: 20),),
                  Text(controller.pokemonModel.evolutions[index],style: TextStyle(color: Colors.grey,fontSize: 16),),
                ],
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Icon(Icons.arrow_forward_ios_sharp,color: colorPokemon,),
                  Icon(Icons.arrow_forward_ios_sharp,color: colorPokemon,),
                  Icon(Icons.arrow_forward_ios_sharp,color: colorPokemon,),
                ],
              ),
              Column(
                children: [
                  CachedNetworkImage(
                    imageUrl: 'https://assets.pokemon.com/assets/cms2/img/pokedex/full/${controller.pokemonModel.evolutions[index+1].substring(1,controller.pokemonModel.evolutions[index].length)}.png',
                    errorWidget: (context, url, error) => Icon(Icons.error),
                    height: 100,
                  ),
                  Text(controller.listEvolution[index+1][0].toUpperCase()+controller.listEvolution[index+1].substring(1,controller.listEvolution[index+1].length),style: TextStyle(color: colorPokemon,fontSize: 20),),
                  Text(controller.pokemonModel.evolutions[index+1],style: TextStyle(color: Colors.grey,fontSize: 16),),
                ],
              ),
            ],
          ),
        );
        }
    );
  }
  Widget _buildBaseStats(){
    Color? colorPokemon =
    AppType.typePokemonColor[controller.pokemonModel.pokemonTypes[0]];
    return Padding(
      padding: EdgeInsets.only(left: 24, right: 24),
      child: Column(
        children: [
          Text(
            'Base Stats',
            style: Get.textTheme.headline2!.copyWith(color: colorPokemon),
          ),
          SizedBox(height: 12),
          BaseStats(
              statsField: 'HP',
              total: 255,
              index: controller.pokemonModel.hp,
              color: colorPokemon),
          BaseStats(
              statsField: 'Attack',
              total: 255,
              index: controller.pokemonModel.attack,
              color: colorPokemon),
          BaseStats(
              statsField: 'Defense',
              total: 255,
              index: controller.pokemonModel.defense,
              color: colorPokemon),
          BaseStats(
              statsField: 'Sp.Atk',
              total: 255,
              index: controller.pokemonModel.specialAttack,
              color: colorPokemon),
          BaseStats(
              statsField: 'Sp.Def',
              total: 255,
              index: controller.pokemonModel.specialDefense,
              color: colorPokemon),
          BaseStats(
              statsField: 'Speed',
              total: 255,
              index: controller.pokemonModel.speed,
              color: colorPokemon),
          BaseStats(
              statsField: 'Total',
              total: 1530,
              index: controller.pokemonModel.total,
              color: colorPokemon),
        ],
      ),
    );
  }
}

class MyCusTomClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    // TODO: implement getClip
    double height = size.height;
    double width = size.width;
    var path = Path();
    path.lineTo(0, height - 50);
    path.quadraticBezierTo(width * .5, height, width, height - 50);
    path.lineTo(width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    // TODO: implement shouldReclip
    return true;
  }
}
