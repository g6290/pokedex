import 'package:get/get.dart';

class AppProvider extends GetConnect {
  Future<Response> getMethod({required String path}) => get(path);
}
