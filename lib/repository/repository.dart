import 'dart:convert';
import 'package:get/get.dart';
import 'package:pokedex/model/ability_model.dart';
import 'package:pokedex/model/list_pokemon_model.dart';
import 'package:pokedex/model/move_model.dart';
import 'package:pokedex/repository/provider.dart';

class PokemonRepository {
  /// ---
  Future<ListPokemonModel> getPokemonGen({required String genId}) async {
    return AppProvider()
        .getMethod(
            path:
                'https://raw.githubusercontent.com/azkals47/pokedex/main/pokemons_gen_$genId.json')
        .then((response) {
      final values = json.decode(response.body);
      return ListPokemonModel.fromJson(values);
    });
  }

  /// ---
  static Future<List<String>> getEvolutionName(
      {required List<String> listId}) async {
    List<String> paths = [];
    listId.forEach((id) => paths.add('https://pokeapi.co/api/v2/pokemon/$id'));
    print(paths);
    var responses = await Future.wait(
        paths.map((path) => AppProvider().getMethod(path: path)).toList());
    return responses.map((response) => getStringFromJson(response)).toList();
  }

  ///
  static String getStringFromJson(Response response) {
    if (response.statusCode == 200) {
      return response.body['name'];
    } else {
      return response.statusCode.toString();
    }
  }

  Future<List<MoveModel>> getMoves() {
    return AppProvider()
        .getMethod(
            path:
                'https://raw.githubusercontent.com/azkals47/pokedex/main/moves.json')
        .then((response) {
      var data = json.decode(response.body);
      return (data['moves'] as List)
          .map((data) => MoveModel.fromJson(data as Map<String, dynamic>))
          .toList();
    });
  }

  /// ---Get ability from api
  Future<List<AbilityModel>> getAbility() async {
    return AppProvider()
        .getMethod(
            path:
                'https://raw.githubusercontent.com/azkals47/pokedex/main/abilites.json')
        .then((response) {
      final values = json.decode(response.body);
      return (values['abilities'] as List)
          .map((data) => AbilityModel.fromJson(data as Map<String, dynamic>))
          .toList();
    });
  }
}
