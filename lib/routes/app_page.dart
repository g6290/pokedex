import 'package:get/get.dart';
import 'package:pokedex/page/ability/presentation/ability_view.dart';
import 'package:pokedex/page/ability_detail/binding/ability_detail_binding.dart';
import 'package:pokedex/page/ability_detail/presentation/ability_detail_view.dart';
import 'package:pokedex/page/dashboard/binding/dashboard_binding.dart';
import 'package:pokedex/page/dashboard/presentation/dashboard_view.dart';
import 'package:pokedex/page/gen_view/binding/gen_view_binding.dart';
import 'package:pokedex/page/gen_view/presentation/gen_view.dart';
import 'package:pokedex/page/gen_view/presentation/search.dart';
import 'package:pokedex/page/move_detail/binding/move_detail_binding.dart';
import 'package:pokedex/page/move_detail/presentation/move_detail.dart';
import 'package:pokedex/page/pokemon_view/binding/pokemon_view_binding.dart';
import 'package:pokedex/page/pokemon_view/presentation/pokemon_view.dart';

import '../page/information/presentation/information_view.dart';

part 'app_route.dart';

class AppPage {
  static const initial = Routes.dashboard;

  static final routes = [
    GetPage(
      name: Routes.dashboard,
      page: () => DashboardPage(),
      binding: DashboardBinding(),
    ),
    GetPage(
      name: Routes.search,
      page: () => Search(),
      binding: GenViewBinding(),
    ),
    GetPage(
      name: Routes.genview,
      page: () => GenView(),
      binding: GenViewBinding(),
    ),
    GetPage(
      name: Routes.pokemonview,
      page: () => PokemonView(),
      binding: PokemonViewBinding(),
    ),
    GetPage(
      name: Routes.movedetail,
      page: () => MoveDetail(),
      binding: MoveDetailBinding(),
    ),
    GetPage(
      name: Routes.ability,
      page: () => AbilityView(),
      binding: DashboardBinding(),
    ),
    GetPage(
      name: Routes.abilityDetail,
      page: () => AbilityDetail(),
      binding: AbilityDetailBinding(),
    ),
    GetPage(
      name: Routes.information,
      page: () => InformationView(),
      binding: DashboardBinding(),
    ),
  ];
}
