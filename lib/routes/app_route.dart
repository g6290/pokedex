part of 'app_page.dart';

abstract class Routes {
  static const dashboard = '/dashboard';
  static const home = '/home';
  static const pokemon = '/pokemon';
  static const search = '/search';
  static const genview = '/genview';
  static const information = '/information';
  static const pokemonview = '/pokemonview';
  static const moveview = '/moveview';
  static const movedetail = '/movedetail';
  static const ability = '/ability';
  static const abilityDetail = '/abilityDetail';
}