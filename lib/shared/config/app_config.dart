abstract class AppConfig {
  static String appName = "Pokedex";

  static const int gridViewSecond = 2;
  static const int gridViewThird = 3;
  static const int gridViewFourth = 4;
}