import 'package:flutter/material.dart';

const String _imagePath = 'assets/png';

class _Image extends AssetImage {
  const _Image(String fileName) : super('$_imagePath/$fileName');
}
class AppImage {
  static const pokeBall = _Image('pokeball.png');
  static const bug = _Image('bug.png');
  static const dark = _Image('dark.png');
  static const dragon = _Image('dragon.png');
  static const electric = _Image('electric.png');
  static const fairy = _Image('fairy.png');
  static const fighting = _Image('fighting.png');
  static const fire = _Image('fire.png');
  static const flying = _Image('flying.png');
  static const ghost = _Image('ghost.png');
  static const grass = _Image('grass.png');
  static const ground = _Image('ground.png');
  static const ice = _Image('ice.png');
  static const normal = _Image('normal.png');
  static const poison = _Image('poison.png');
  static const psychic = _Image('psychic.png');
  static const rock = _Image('rock.png');
  static const steel = _Image('steel.png');
  static const water = _Image('water.png');
  static const pokedex = _Image('pokedex.png');

  static _Image? findImageType(String nameType){
    switch(nameType){
      case'bug': return AppImage.bug;
      case'dark': return AppImage.dark;
      case'dragon': return AppImage.dragon;
      case'electric': return AppImage.electric;
      case'fairy': return AppImage.fairy;
      case'fighting': return AppImage.fighting;
      case'fire': return AppImage.fire;
      case'flying': return AppImage.flying;
      case'ghost': return AppImage.ghost;
      case'grass': return AppImage.grass;
      case'ground': return AppImage.ground;
      case'normal': return AppImage.normal;
      case'poison': return AppImage.poison;
      case'psychic': return AppImage.psychic;
      case'rock': return AppImage.rock;
      case'steel': return AppImage.steel;
      case'water': return AppImage.water;
      case'ice': return AppImage.ice;
      case'pokedex': return AppImage.pokedex;
    }
  }
}