import 'package:flutter/cupertino.dart';
import 'package:pokedex/model/gen_model.dart';
import 'package:pokedex/shared/theme/style.dart';

abstract class AppType {
 static List<GenModel> genList = [
   GenModel(id: 'i', name: 'Gen I', color: Styles.greenColor),
   GenModel(id: 'ii', name: 'Gen II', color: Styles.redColor),
   GenModel(id: 'iii', name: 'Gen III', color: Styles.blueColor),
   GenModel(id: 'iv', name: 'Gen IV', color: Styles.yellowColor),
   GenModel(id: 'v', name: 'Gen V', color: Styles.purpleColor),
   GenModel(id: 'vi', name: 'Gen VI', color: Styles.brownColor),
 ];

 static String normal ='Normal';
 static String dark ='Dark';
 static String ground ='Ground';
 static String steel ='Steel';
 static String water ='Water';
 static String rock ='Rock';
 static String grass='Grass';
 static String psychic ='Psychic';
 static String poison ='Poison';
 static String bug ='Bug';
 static String electric ='Electric';
 static String flying ='Flying';
 static String fairy ='Fairy';
 static String ghost ='Ghost';
 static String fighting ='Fighting';
 static String dragon ='Dragon';
 static String fire ='Fire';
 static String ice ='Ice';
 static Map<String,Color> typePokemonColor = {
   normal: Styles.normalColor,
   dark: Styles.darkColor,
   ground: Styles.groundColor,
   steel: Styles.steelColor,
   water: Styles.waterColor,
   rock: Styles.rockColor,
   grass: Styles.grassColor,
   psychic: Styles.psychicColor,
   poison: Styles.poisonColor,
   bug: Styles.bugColor,
   electric: Styles.electricColor,
   flying: Styles.flyingColor,
   fairy: Styles.fairyColor,
   fighting: Styles.fightingColor,
   dragon: Styles.dragonColor,
   fire: Styles.fireColor,
   ice: Styles.iceColor,
   ghost: Styles.ghostColor,
 };

 static String one ='3';
 static String two ='4';
 static String three ='5';
 static String four ='6';
 static String five ='7';
 static Map<String,Color> generationIntroducedColor = {
   one: Styles.blueColor,
   two: Styles.greenColor,
   three: Styles.rockColor,
   four: Styles.normalColor,
   five: Styles.iceColor,
 };
}