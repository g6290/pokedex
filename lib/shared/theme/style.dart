import 'package:flutter/material.dart';

abstract class Styles {
  static const Color greenColor = Color(0xFF4DC2a7);
  static const Color redColor = Color(0xFFFB576A);
  static const Color blueColor = Color(0xFF57ABF2);
  static const Color yellowColor = Color(0xFFFFCE4B);
  static const Color purpleColor = Color(0xFF7C538C);
  static const Color brownColor = Color(0xFFB1736A);
  static const Color whiteColor = Colors.white;
  static const Color blackColor = Colors.black87;

  static const Color textColorWhite = Colors.white;
  static const Color textColorBlack = Colors.black87;

  static const Color normalColor =	Color(0xFFA8A878);
  static const Color darkColor	=	Color(0xFF303943);
  static const Color groundColor	= Color(0xFF59403b);
  static const Color steelColor	=	Color(0x64303943);
  static const Color waterColor	=	Color(0xFF7AC7FF);
  static const Color rockColor	=	Color(0xFFCA8179);
  static const Color grassColor	=	Color(0xFF4a9158);
  static const Color psychicColor	=	Color(0xFFEE99AC);
  static const Color poisonColor	=	Color(0xFF9F5BBA);
  static const Color bugColor =	Color(0xFF48D0B0);
  static const Color electricColor	=	Color(0xFFFFCE4B);
  static const Color flyingColor	=	Color(0xFFA890F0);
  static const Color fairyColor	=	Color(0xFFF85888);
  static const Color ghostColor	=	Color(0xFF7C538C);
  static const Color fightingColor	=	Color(0xFFFA6555);
  static const Color dragonColor	=	Color(0xD07038F8);
  static const Color fireColor	=	Color(0xFFFB6C6C);
  static const Color iceColor	=	Color(0xFF98d9d8);
}