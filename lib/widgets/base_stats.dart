import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:pokedex/widgets/vertial_devider.dart';

class BaseStats extends GetView {
  const BaseStats({
    Key? key,
    required this.statsField,
    required this.index,
    required this.color,
    required this.total
  }) : super(key: key);
  final String statsField;
  final int index;
  final int total;
  final Color? color;
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          alignment: Alignment.centerLeft,
          height: 32,
          width: 64,
          child: Text(
            statsField,
            style: Get.textTheme.headline5!.copyWith(color: color),
          ),
        ),
        CustomVerticalDivider(height: 32,color: color,),
        Container(
          alignment: Alignment.center,
          height: 32,
          width: 36,
          child: Text(
            '$index',
            style: Get.textTheme.headline5!.copyWith(color: color),
          ),
        ),
        Container(
          alignment: Alignment.center,
          height: 32,
          child: Container(
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
                color: Colors.grey.withOpacity(0.5),
                borderRadius: BorderRadius.circular(32)),
            height: 8,
            width: Get.width - 158,
            child: Container(
              decoration: BoxDecoration(
                  color: color,
                  borderRadius: BorderRadius.circular(32)),
              height: 8,
              width: ((Get.width - 158)*index)/total,
            ),
          ),
        ),
      ],
    );
  }
}
