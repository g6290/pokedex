import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  const CustomAppBar({Key? key,this.namePage='home'}) : super(key: key);
  final String namePage;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      flexibleSpace: Container(
        alignment: Alignment.centerLeft,
        height: Get.height,
        padding: EdgeInsets.only(left: 48),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.topRight,
            colors: [Colors.lightBlueAccent, Colors.greenAccent],
          ),
        ),
      ),
      title: Text(namePage.tr)
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}
