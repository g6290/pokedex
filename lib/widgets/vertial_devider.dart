import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomVerticalDivider extends StatelessWidget {
  const CustomVerticalDivider({
    Key? key,
    this.height = 32,
    this.thickness = 2,
    this.indent = 0,
    this.endIndent = 0,
    this.width = 10,
    this.color = Colors.black,
  }) : super(key: key);
  final double height;
  final double thickness;
  final double indent;
  final double endIndent;
  final double width;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      child: VerticalDivider(
        color: color,
        indent: indent,
        endIndent: endIndent,
        thickness: thickness,
      ),
    );
  }
}
